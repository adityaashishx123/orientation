
**GitLab**

*GitLab is a github like service that organizations can use to provide internal management of git repositories. *
*It is a self hosted Git-repository management system that keeps the user code private and can easily deploy the changes of the code.*

1.It helps to hosts your projects for free.

2.It offers free public and private repositories, issue-tracking and wikis.

3.It is a user friendly web interface layer on top of Git.

**Advantages**

1.GitLab provides unlimited number of private and public repositories for free.

2.GitLab provides GitLab Community Edition version for users to locate, on which servers their code is present.

**Disadvantages**

1.GitLab is not fast as GitHub when pushing and pulling repositories.

2.GitLab interface  takes time while switching from one to another page.