
**Git**

Git is a distributed version control system designed to handle  projects with speed and efficiency.

Its a platform to share work globally to the community so that other developers from the world can use for their purpose.

**Functions of Git**


> Repository

*A repository contains all of the project files , and stores each file's revision history. *
*Repositories can have multiple collaborators and can be either public or private.*


> Commit

*A commit is an individual change to a file .*

> Branch

*A branch is essentially is a unique set of code changes with a unique name.*

> Clone

*Clone command downloads an existing Git repository to your local computer.*


> Fork

*Fork command  produces a personal copy of someone other person's project.*

> Push

*The push command is used to upload local repository content to a remote repository. *

> Pull

* Pull command  lets  other tell  about changes you've pushed to a Git repository.*

> Merge

*The  merge command lets you take the independent lines of development created by git branch and integrate them into a single branch.*